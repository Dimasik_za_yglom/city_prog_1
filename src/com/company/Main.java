package com.company;

import com.company.utils.CityUtils;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Path path = Paths.get("document/city_ru.csv");

        try {
            Scanner scanner = new Scanner(path);
            CityUtils.outMapCity(CityUtils.createdListCity(scanner));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
