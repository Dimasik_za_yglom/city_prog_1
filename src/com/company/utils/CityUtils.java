package com.company.utils;

import com.company.entity.City;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class CityUtils {

    public static List<City> createdListCity(Scanner scanner) {
        List<City> listCity = new LinkedList<>();
        while (scanner.hasNextLine()) {
            listCity.add(createCityOfFile(scanner.nextLine()));
        }
        scanner.close();
        return listCity;
    }

    private static City createCityOfFile(String line) {
        String[] parametrCity = line.split(";");
        return new City(parametrCity[1], parametrCity[2], parametrCity[3], Integer.valueOf(parametrCity[4]),
                parametrCity.length == 6 ? parametrCity[5] : null);
    }

    public static void outMapCity(List<City> mapCity) {
        mapCity.forEach(System.out::println);
    }
}
